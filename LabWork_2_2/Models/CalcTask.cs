﻿namespace LabWork_2_2.Models
{
    public class Operation
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class CalcTask
    {
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }
        public int OperationId { get; set; }

    }
}
