﻿using LabWork_2_2.Models;
using System;

namespace LabWork_2_2.Services
{
    public class Generator : ITaskGenerator
    {
        public CalcTask GetTask()
        {
            Random Rnd = new Random();
            int a = Rnd.Next(0, 10);
            int b = Rnd.Next(0, 10);
            CalcTask task = new CalcTask { FirstNumber = a, SecondNumber = b};
            return task;
        }
    }
}
