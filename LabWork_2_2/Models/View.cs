﻿namespace LabWork_2_2.Models
{
    public class View
    {
        public CalcTask Task { get; set; }
        public Solution Solution { get; set; }

        public View()
        {
            Task = new CalcTask();
            Solution = new Solution();
        }
    }
}
