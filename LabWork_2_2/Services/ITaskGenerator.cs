﻿using LabWork_2_2.Models;
using System;

namespace LabWork_2_2.Services
{
    public interface ITaskGenerator
    {
        public CalcTask GetTask();
    }
}
