﻿namespace LabWork_2_2.Models
{
    public class Solution
    {
        public string Add { get; set; }
        public string Sub { get; set; }
        public string Mult { get; set; }
        public string Div { get; set; }
    }
}
