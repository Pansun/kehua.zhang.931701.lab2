﻿using LabWork_2_2.Models;

namespace LabWork_2_2.Services
{
    public interface ICalculator
    {
        public string Solution(CalcTask task);
        public string AddSolution(CalcTask task);
        public string SubSolution(CalcTask task);
        public string MultSolution(CalcTask task);
        public string DivSolution(CalcTask task);

    }
}
