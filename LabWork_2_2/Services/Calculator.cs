﻿using System;
using LabWork_2_2.Models;

namespace LabWork_2_2.Services
{
    public class Calculator : ICalculator
    {
        public string Solution(CalcTask task)
        {
            switch (task.OperationId)
            {
                case 1:
                    return this.AddSolution(task);
                case 2:
                    return this.SubSolution(task);
                case 3:
                    return this.MultSolution(task);
                case 4:
                    return this.DivSolution(task);
            }
            return "Error";
        }

        public string AddSolution(CalcTask task)
        {
            string res;
            if (task.SecondNumber >= 0)
                res = task.FirstNumber.ToString() + " + " + task.SecondNumber.ToString() + " = ";
            else
                res = task.FirstNumber.ToString() + " - " + Math.Abs(task.SecondNumber).ToString() + " = ";
            return res + (task.FirstNumber + task.SecondNumber).ToString();
        }
        public string SubSolution(CalcTask task)
        {
            string res;
            if (task.SecondNumber >= 0)
                res = task.FirstNumber.ToString() + " - " + task.SecondNumber.ToString() + " = ";
            else
                res = task.FirstNumber.ToString() + " + " + Math.Abs(task.SecondNumber).ToString() + " = ";
            return res + (task.FirstNumber - task.SecondNumber).ToString();
        }
        public string MultSolution(CalcTask task)
        {
            string res;
            res = task.FirstNumber.ToString() + " * " + task.SecondNumber.ToString() + " = ";
            return res + (task.FirstNumber * task.SecondNumber).ToString();
        }
        public string DivSolution(CalcTask task)
        {
            //обработка ошибок
            if (task.SecondNumber != 0)
            {
                string res;
                res = task.FirstNumber.ToString() + " / " + task.SecondNumber.ToString() + " = ";
                return res + (task.FirstNumber / task.SecondNumber).ToString();
            }
            else
                return "Error - Division by zero";
        }
    }
}
