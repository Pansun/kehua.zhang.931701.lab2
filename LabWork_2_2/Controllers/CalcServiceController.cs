﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using LabWork_2_2.Services;
using LabWork_2_2.Models;


namespace LabWork_2_2.Controllers
{
    public class CalcServiceController : Controller
    {

        private readonly ICalculator _calculator;
        private readonly ITaskGenerator _generator;
        public CalcServiceController(ICalculator calculator, ITaskGenerator generator)
        {
            this._calculator = calculator;
            this._generator = generator;
        }

        IEnumerable<Operation> operations = new List<Operation>
        {
            new Operation { Id = 1, Name = "+" },
            new Operation { Id = 2, Name = "-" },
            new Operation { Id = 3, Name = "*" },
            new Operation { Id = 4, Name = "/"}
        };

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ModelBinding()
        {
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            return View();
        }

        [HttpPost]
        public IActionResult ModelBinding(CalcTask task)
        {
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            ViewBag.Result = _calculator.Solution(task);
            return View();
        }

        public IActionResult Manual()
        {
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            return View();
        }

        [HttpPost]
        public IActionResult Manual(int FirstNumber, int SecondNumber, int OperationId)
        {
            CalcTask task = new CalcTask { FirstNumber = FirstNumber, SecondNumber = SecondNumber, OperationId = OperationId };
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            ViewBag.Result = _calculator.Solution(task);
            return View();
        }

        public IActionResult Inject()
        {
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            return View();
        }

        [HttpPost]
        public IActionResult Inject(CalcTask task)
        {
            ViewBag.Operations = new SelectList(operations, "Id", "Name");
            ViewBag.Task = task;
            return View();
        }
    }
}
