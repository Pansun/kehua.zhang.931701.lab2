﻿using Microsoft.AspNetCore.Mvc;

namespace LabWork_2_2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
